DROP TABLE IF EXISTS course_violations_per_student_item;

CREATE TABLE course_violations_per_student_item (
    course_branch_id VARCHAR(50)
    ,eitdigital_id VARCHAR(50),
    course_module_id VARCHAR(50),
    course_lesson_id VARCHAR(50),
    course_item_id VARCHAR(50)
    ,violations INT4
    ,no_violationss INT4
    ,going_back INT4
);

