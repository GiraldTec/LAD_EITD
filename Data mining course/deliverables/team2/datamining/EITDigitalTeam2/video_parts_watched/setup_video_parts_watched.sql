﻿DROP TABLE IF EXISTS video_parts_watched;

CREATE TABLE video_parts_watched(
  course_item_id character varying,
  watched_part numrange,
  hashed_user_id character varying
);

CREATE INDEX video_parts_watched_hashed_user_id ON video_parts_watched (hashed_user_id);
CREATE INDEX video_parts_watched_course_item_id_idx ON video_parts_watched (course_item_id);

CREATE OR REPLACE VIEW video_parts_watched_learners AS
  SELECT course_item_id, watched_part, hashed_user_id, COUNT(*) AS times_watched
  FROM video_parts_watched
  GROUP BY course_item_id, watched_part, hashed_user_id;