import psycopg2
import math
from psycopg2.extras import NumericRange

COURSE_ITEM_ID = 0
HASHED_USER_ID = 1
SERVER_TIMESTAMP = 2
#KEY = 3
TIMECODE = 3

def contiguous_heartbeats(hb1, hb2):
    contiguous_threshold = 10
    timestamp_diff = hb2[SERVER_TIMESTAMP] - hb1[SERVER_TIMESTAMP]

    return (
        hb1[COURSE_ITEM_ID] == hb2[COURSE_ITEM_ID] and
        hb1[HASHED_USER_ID] == hb2[HASHED_USER_ID] and
        hb1[TIMECODE] <= hb2[TIMECODE] and
        hb2[TIMECODE] - hb1[TIMECODE] < contiguous_threshold and
        timestamp_diff.total_seconds() < contiguous_threshold
    )

def register_heartbeat_range(cur, hbr):
    #actual timecode range that the learner has watched in the video
    hbr_from = max(0,hbr[0][TIMECODE]-5)
    hbr_to = hbr[1][TIMECODE]

    #bin the values of the range
    bin_size = 10
    hbr_bin_from = math.floor(hbr_from / bin_size) * bin_size
    hbr_bin_to = math.ceil(hbr_to / bin_size) * bin_size
    watched_parts = []
    for n in range(math.floor(hbr_bin_from), math.floor(hbr_bin_to), bin_size):
        watched_parts.append(NumericRange(n, n+bin_size))

    for r in watched_parts:
        cur.execute(
            """INSERT INTO video_parts_watched(course_item_id, watched_part, hashed_user_id)
                VALUES(%s, %s, %s);""", (
            hbr[0][COURSE_ITEM_ID],
            r,
            hbr[0][HASHED_USER_ID]
        ))

conn = psycopg2.connect("dbname=datamining user=postgres")
cur = conn.cursor()

cur.execute("DECLARE clickstream_events_video_unpacked_cursor CURSOR FOR SELECT * FROM clickstream_events_video_unpacked")

cur2 = conn.cursor("clickstream_events_video_unpacked_cursor")

c_heartbeat_range = None
count = 0

for record in cur2:
#    (course_item_id, hashed_user_id, server_timestamp, key, timecode) = record
    (course_item_id, hashed_user_id, server_timestamp, timecode) = record
    if c_heartbeat_range is None:
#        if key == 'heartbeat':
        c_heartbeat_range = (record,record)
    else:
#        if key == 'heartbeat':
        if contiguous_heartbeats(c_heartbeat_range[1], record):
            c_heartbeat_range = (c_heartbeat_range[0], record)
        else:
            register_heartbeat_range(cur,c_heartbeat_range)
            c_heartbeat_range = (record,record)
#        else:
#            register_heartbeat_range(cur,c_heartbeat_range)
#            c_heartbeat_range = None

    count += 1
    if count % 10000 == 0:
        print(count)

conn.commit()

cur.close()
conn.close()
