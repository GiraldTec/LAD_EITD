﻿DROP TABLE IF EXISTS video_list;

SELECT
  course_id, 
  course_item_id AS item_id,
  concat_ws(' / ', course_name, course_branch_module_name, course_branch_lesson_name, course_branch_item_name) AS item_name
INTO video_list
FROM courses
LEFT JOIN course_branch_modules ON courses.course_id = course_branch_modules.course_branch_id
LEFT JOIN course_branch_lessons ON course_branch_modules.course_module_id = course_branch_lessons.course_module_id
LEFT JOIN course_branch_items ON course_branch_lessons.course_lesson_id = course_branch_items.course_lesson_id
WHERE course_item_type_id = 1 AND course_item_id IS NOT NULL

ORDER BY course_id, course_branch_modules.course_branch_module_order, course_branch_lessons.course_branch_lesson_order, course_branch_items.course_branch_item_order;