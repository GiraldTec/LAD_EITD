DROP TABLE IF EXISTS clickstream_events_video_unpacked;

SELECT
  value->>'item_id' AS course_item_id,
  hashed_user_id,
  server_timestamp,
  cast(nullif(value->>'timecode','') as float) AS timecode
INTO clickstream_events_video_unpacked
FROM clickstream_events_video
WHERE key = 'heartbeat'
ORDER BY course_item_id, hashed_user_id, server_timestamp;
