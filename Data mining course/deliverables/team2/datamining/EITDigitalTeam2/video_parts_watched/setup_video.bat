psql -U postgres -d datamining -f setup_clickstream_events_video.sql
psql -U postgres -d datamining -f setup_clickstream_events_video_unpacked.sql
psql -U postgres -d datamining -f setup_video_parts_watched.sql
python bin_heartbeats.py
psql -U postgres -d datamining -f setup_video_parts_watched_item.sql
psql -U postgres -d datamining -f setup_learner_selection.sql
psql -U postgres -d datamining -f setup_video_list.sql
